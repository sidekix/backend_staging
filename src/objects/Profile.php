<?php
/**
 * User: jerry
 * Date: 1/11/15
 * Time: 6:33 AM
 */

require_once __DIR__ . "/../database_functions/ProfileDAO.php";
require_once __DIR__ . "/../database_functions/ReviewDAO.php";
require_once __DIR__ . "/../database_functions/PhotoDAO.php";

require_once("SideKixObject.php");

class Profile extends SideKixObject{

    private $username=null;

    private $password = null;

    private $fbid = null;

    private $first_name = null;

    private $last_name = null;

    private $create_time = null;

    private $last_login_time = null;

    private $admin = null;

    private $photo_id = null;

    private $bio = null;

    private $html_link = null;

    private $last_geotag = null;

    public function __construct ($fields)
    {
        $this->checkValidFields($fields, $GLOBALS["TABLE_PROFILES_SCHEMA"], $GLOBALS["TABLE_PROFILES_SCHEMA_TYPE"]);

        parent::__construct($fields);

        $this->username = $fields["username"];

        $this->fbid = $fields["fbid"];

        $this->first_name = $fields["first_name"];

        $this->last_name = $fields["last_name"];

        $this->create_time = $fields["create_time"];

        $this->last_login_time = $fields["last_login_time"];

        $this->admin = $fields["admin"];

        $this->photo_id = $fields["photo_id"];

        $this->bio = $fields["bio"];

        $this->html_link = $fields["html_link"];

        $this->last_geotag = $fields["last_geotag"];

        $this->LOG = new SideKix_LOGGER(__CLASS__);

    }

    public function get_username()
    {
        return $this->username;
    }

    public function get_fbid()
    {
        return $this->fbid;
    }

    public function get_first_name()
    {
        return $this->first_name;
    }

    public function get_last_name()
    {
        return $this->last_name;
    }

    public function get_create_time()
    {
        return $this->create_time;
    }

    public function get_last_login_time()
    {
        return $this->last_login_time;
    }

    public function get_admin()
    {
        return $this->admin;
    }

    public function get_photo_id()
    {
        return $this->photo_id;
    }

    public function get_bio()
    {
        return $this->bio;
    }

    public function get_html_link()
    {
        return $this->html_link;
    }

    public function get_last_geotag()
    {
        return $this->last_geotag;
    }

    public static function getProfileWithId($profile_id)
    {
        HelperFuncs::checkType($profile_id, $GLOBALS ["TABLE_PROFILES_SCHEMA_TYPE"]["id"],__CLASS__, __METHOD__);
        $profilesDAO = new ProfileDAO();
        $results = $profilesDAO->getProfileWithId($profile_id);

        $profile_result = new Profile($results);

        return $profile_result;
    }

    /**
     * fetches all the info needed to generate a complete profile.
     *
     * @author jerry
     * @param unknown $profile_id
     */
    public static function fetch_complete_profile($profile_id)
    {
        if (gettype($profile_id) != $GLOBALS["TABLE_PROFILES_SCHEMA_TYPE"]["id"]) {
            throw new InvalidArgumentException("profile_id type incorrect");
        }

        $profileDAO = new ProfileDAO();
        $json_data = array();
        $profile_data = $profileDAO->getProfileWithId($profile_id);

        //var_dump($profile_data);

        $json_data ["profile_data"] = array(
            "profile_id" => $profile_data [$GLOBALS ["TABLE_PROFILES_SCHEMA"] ["id"]],
            "username" => $profile_data [$GLOBALS ["TABLE_PROFILES_SCHEMA"] ["username"]],
            "first_name" => $profile_data [$GLOBALS ["TABLE_PROFILES_SCHEMA"] ["first_name"]],
            "last_name" => $profile_data [$GLOBALS ["TABLE_PROFILES_SCHEMA"] ["last_name"]],
            "photo_id" => $profile_data [$GLOBALS ["TABLE_PROFILES_SCHEMA"] ["photo_id"]],
            "bio" => $profile_data [$GLOBALS ["TABLE_PROFILES_SCHEMA"] ["bio"]],
            "last_geotag" => $profile_data [$GLOBALS ["TABLE_PROFILES_SCHEMA"] ["last_geotag"]],
            "html_link" => $profile_data [$GLOBALS ["TABLE_PROFILES_SCHEMA"] ["html_link"]]
        );

        $json_data ["distance"] = null;
        $json_data ["avg_rating"] = "?";
        $json_data ["num_likes"] = "?";

        $json_data ["skills"] = array();
        $skills = $profileDAO->fetchProfileSkills($profile_id);
        // $LOG->debug ( $GLOBALS ["TABLE_SKILLS_SCHEMA"] ["name"] );
        //var_dump($skills);

        foreach ($skills as $skill) {
            $r = new SideKix_LOGGER(__CLASS__);
            $r->debug("/** Skill id:".$skill["id"]."  **/");
            $skill_photos = $profileDAO->fetchSkillPhotos(intval($skill["profile_skill_id"]));
            $skill_reviews = $profileDAO->fetchSkillReviews(intval($skill["profile_skill_id"]));

            $data_row = array(
                "id" => $skill [$GLOBALS ["TABLE_SKILLS_SCHEMA"] ["id"]],
                "skill_name" => $skill [$GLOBALS ["TABLE_SKILLS_SCHEMA"] ["name"]],
                "user_description" => $skill ["description"],
                "bookmarked_by_viewer" => "?",
                "photos" => $skill_photos,
                "reviews" => $skill_reviews
            );

            array_push($json_data ["skills"], $data_row);
        }

        //var_dump($json_data);
        return json_encode($json_data);
        //var_dump ( json_decode ( json_encode ( $json_data ) ) );
    }

    /**
     * @param $user_profile_id
     * @param $visited_profile_id
     */
    function fetchVisitProfile($user_profile_id, $visited_profile_id)
    {

    }

    function modifyProfileBasic($action, $authentication) {

        HelperFuncs::assertNotNull($action, array("owner_id", "type", "subject", "edits"));

        $profileDAO = new ProfileDAO();
        $owner_id = $action["owner_id"];
        $type = $action["type"];
        $subject = $action["subject"];
        $edits = $action["edits"];
        $profileDAO->modifyProfileBasic($owner_id, $edits);



    }
}

?>