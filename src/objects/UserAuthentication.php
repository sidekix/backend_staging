<?php
/**
 * Created by PhpStorm.
 * User: jerry
 * Date: 1/21/15
 * Time: 12:08 PM
 */

require_once __DIR__."/../Config.php";
require_once(__DIR__."/../aux_functions/HelperFuncs.php");
require_once(__DIR__."/../database_functions/UserAuthenticationDAO.php");


class UserAuthentication {

    public static function authenticateWithUsernamePassword($username, $password)
    {
        HelperFuncs::checkType($username, STR, __CLASS__, __METHOD__);
        HelperFuncs::checkType($username, STR, __CLASS__, __METHOD__);
        $dao = new UserAuthenticationDAO();
        $token = $dao->authenticateCredentialsWithSidkixDB($username, $password);
        if($token == null) {
            return "invalid";
        }

        return $token;
    }

    public static function authenticateWithAccessToken($username, $token)
    {
        HelperFuncs::checkType($token, STR, __CLASS__, __METHOD__);

        $dao = new UserAuthenticationDAO();
        if($dao->authenticateWithToken($username, $token) == false) {
            throw new Exception ("Not Authorized");
        }

    }

}