<?php

/**
 * Created by PhpStorm.
 * User: jerry
 * Date: 1/17/15
 * Time: 11:33 AM
 */
abstract class SideKixObject
{

    /**
     * object id;
     */
    protected $id = null;

    protected $fields = null;

    protected $LOG=null;

    protected function __construct($fields)
    {
        $this->id = $fields["id"];
        $this->fields = $fields;
        $this->LOG = new SideKix_LOGGER(__CLASS__);
    }

    static public function checkValidFields($fields, $schema_table, $schema_type_table)
    {
        if (gettype($fields) != "array" && gettype($schema_table) != "array" && gettype($schema_type_table) != "array") {
            throw new InvalidArgumentException("invalid arguments in " . __CLASS__ . ":" . __METHOD__);
        }
        foreach ($schema_table as $key => $val) {
            if (array_key_exists($key, $fields) == false) {

                echo "fields: " .var_dump($fields);
                throw new InvalidArgumentException("Invalid field: " . $key. " ". __CLASS__ . ":" . __METHOD__);
            }
        }
    }

    public function returnField($field)
    {
        if (gettype($field) != "string") {
            throw new InvalidArgumentException("invalid arguments in " . __CLASS__ . ":" . __METHOD__);
        }
        if (array_key_exists($field, $this->fields[$field]) == false) {
            throw new InvalidArgumentException("Expected field: " . $field . " not defined");
        } else {

            return $this->fields[$field];
        }

    }

    public function get_id()
    {
        return $this->id;
    }

    public function __toString()
    {
        return json_encode($this->fields);
    }

    public function toArray()
    {
        return $this->fields;
    }
}


?>