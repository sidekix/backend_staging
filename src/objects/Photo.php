<?php
/**
 * Created by PhpStorm.
 * User: jerry
 * Date: 1/24/15
 * Time: 8:09 AM
 */

require_once("SideKixObject.php");
require_once(__DIR__ . "/../database_functions/PhotoDAO.php");
require_once(__DIR__."/../aux_functions/HelperFuncs.php");

class Photo extends SideKixObject {

    private $profile_id =null;

    private $upload_time = null;

    private $file_path = null;

    private $geotag = null;

    private $views = null;

    private $likes = null;

    public function __construct($fields)
    {
        $this->checkValidFields($fields, $GLOBALS["TABLE_PHOTOS_SCHEMA"], $GLOBALS["TABLE_PHOTOS_SCHEMA_TYPE"]);

        parent::__construct($fields);

        $this->profile_id = $fields["profile_id"];

        $this->upload_time = $fields["upload_time"];

        $this->file_path = $fields["file_path"];

        $this->geotag = $fields["geotag"];

        $this->views = $fields["views"];

        $this->likes = $fields["likes"];
    }

    public function get_profile_id()
    {
        return $this->profile_id;
    }

    public function get_upload_time()
    {
        return $this->upload_time;
    }

    public function get_file_path()
    {
        return $this->file_path;
    }

    public function get_geotag()
    {
        return $this->geotag;
    }

    public function get_views()
    {
        return $this->views;
    }

    public function get_likes()
    {
        return $this->likes;
    }

    public static function getPhotoWithId($profile_id)
    {
        HelperFuncs::checkType($profile_id, $GLOBALS ["TABLE_PHOTOS_SCHEMA_TYPE"]["id"],__CLASS__, __METHOD__);
        $photoDAO = new PhotoDAO();
        $results = $photoDAO->getPhotoWithId($profile_id);
        $photo_result = new Photo($results);
        return $photo_result;
    }

}
?>