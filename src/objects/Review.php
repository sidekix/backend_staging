<?php
/**
 * User: jerry
 * Date: 1/15/15
 * Time: 5:31 PM
 */

require_once("SideKixObject.php");
require_once(__DIR__ . "/../database_functions/ReviewDAO.php");
require_once(__DIR__."/../aux_functions/HelperFuncs.php");

class Review extends SideKixObject{

    private $reviewer_profile_id = null;

    private $time = null;

    private $review = null;

    private $stars = null;


    public function __construct($fields)
    {

        $this->checkValidFields($fields, $GLOBALS["TABLE_REVIEWS_SCHEMA"], $GLOBALS["TABLE_REVIEWS_SCHEMA_TYPE"]);

        parent::__construct($fields);

        $this->reviewer_profile_id = $fields["reviewer_profile_id"];
        $this->time = $fields["time"];
        $this->review = $fields["review"];
        $this->stars = $fields["stars"];
    }

    public function get_reviewer_profile_id() {
        return $this->reviewer_profile_id;
    }

    public function get_time() {
        return $this->time;
    }

    public function get_review() {
        return $this->review;
    }

    public function get_stars() {
        return $this->stars;
    }

    public static function getReviewWithId($review_id) {

        HelperFuncs::checkType($review_id, $GLOBALS ["TABLE_REVIEWS_SCHEMA_TYPE"]["id"],__CLASS__, __METHOD__);
        $reviewsDAO = new ReviewDAO();
        $results = $reviewsDAO->getReviewWithId($review_id);
        $review_result = new Review($results);
        return $review_result;

    }
}
