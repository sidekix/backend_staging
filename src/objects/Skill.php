<?php
/**
 * Created by PhpStorm.
 * User: jerry
 * Date: 1/24/15
 * Time: 6:51 AM
 */

require_once("SideKixObject.php");
require_once(__DIR__ . "/../database_functions/SkillDAO.php");
require_once(__DIR__."/../aux_functions/HelperFuncs.php");

class Skill extends SideKixObject {

    private $name = null;

    private $is_predefined = null;

    private $time_created = null;

    private $creator_profile_id = null;

    public function __construct($fields) {

        $this->checkValidFields($fields, $GLOBALS["TABLE_SKILLS_SCHEMA"], $GLOBALS["TABLE_SKILLS_SCHEMA_TYPE"]);

        parent::__construct($fields);

        $this->name = $fields["name"];

        $this->is_predefined = $fields["is_predefined"];

        $this->time_created = $fields["time_created"];

        $this->creator_profile_id = $fields["creator_profile_id"];

    }

    public function get_name()
    {
        return $this->name;
    }

    public function get_is_predefined()
    {
        return $this->is_predefined;
    }

    public function get_time_created()
    {
        return $this->time_created;
    }

    public function get_creator_profile_id()
    {
        return $this->creator_profile_id;
    }

    public static function getSkillWithId($skill_id)
    {
        HelperFuncs::checkType($skill_id, $GLOBALS ["TABLE_SKILLS_SCHEMA_TYPE"]["id"],__CLASS__, __METHOD__);
        $skillDAO = new SkillDAO();
        $results = $skillDAO->getSkillWithId($skill_id);
        $skill_result = new Skill($results);
        return $skill_result;
    }
}

?>