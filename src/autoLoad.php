<?php

spl_autoload_register(
    function ($class) {
        require_once __DIR__ . '/database_functions/DBConfig.php';
        require __DIR__ . '/database_functions/calculations/LocationBasedCalc.php';
        require __DIR__ . '/database_functions/SideKixDBAccessObject.php';



//        static $classes = null;
//        if ($classes === null) {
//            $classes = array(
//                'database_functions\\calculations\\LocationBasedCalc;' => '/database_functions/calculations/LocationBasedCalc.php'
//            );
//        }
//        $cn = strtolower($class);
//        if (isset($classes[$cn])) {
//            require __DIR__ . $classes[$cn];
//        }
    }
);
// @codeCoverageIgnoreEnd