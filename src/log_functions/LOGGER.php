<?php
/**
 * log_functions.php
 * Contains the functions for logging
 * @author jerry
 */

require_once(__DIR__."/../Config.php");
include(LOG4PHP_PATH.'/Logger.php');

/**
 *
 */
Logger::configure(array(
	'rootLogger' => array(
		'appenders' => array('default'),
	),
	'appenders' => array(
		'default' => array(
			'class' => 'LoggerAppenderFile',
			'layout' => array(
				'class' => 'LoggerLayoutPattern',
				'params' => array(
					'conversionPattern' => "%date [%logger] [%level] %ex- %message%newline"
				),
			),
			'params' => array(
				'file' => LOG_PATH.'/main.log',
				'append' => true
			)
		)
	)
));


/**
 * Class for Logging
 * 
 * @author jerry
 */
class SideKix_LOGGER {

	/** Holds the Logger. */
	private $log;

	/** Logger is instantiated in the constructor. */
	public function __construct($object)
	{
		// The __CLASS__ constant holds the class name
		$this->log = Logger::getLogger($object);
	}

	public function error($msg) {
		$this->log->error($msg);
	}
	public function debug($msg) {
		$this->log->debug($msg, null);
	}
	public function info($msg) {
		$this->log->info($msg);
	}
	public function warning($msg) {
		$this->log->warn($msg);
	}
}
?>