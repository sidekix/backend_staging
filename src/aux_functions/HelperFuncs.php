<?php
/**
 * Created by PhpStorm.
 * User: jerry
 * Date: 1/21/15
 * Time: 12:10 PM
 */

class HelperFuncs {

    public static function checkType($var, $type, $class, $method)
    {
        if ($var != null && gettype($var) != $type) {
            throw new InvalidArgumentException("invalid arguments in " . $class . ":" . $method);
        }
    }

    public static function attachTransactionSuccessHeader($msg_json)
    {
        $msg = array();
        $decoded = json_decode($msg_json, true);
        if($decoded == null) {
            throw new InvalidArgumentException("Cannot decode msg json!");
        }

        $msg["status"] = 0;
        $msg["status_message"] = "successful query";
        $msg["data"] = $decoded;
        return json_encode($msg);
    }

    public static function getFailureMessageHeader($error_msg, $error_number) {
        if(gettype($error_msg) != "string" || gettype($error_number) != "integer") {
            throw new InvalidArgumentException("Function getFailureMessageHeader arguments invalid");
        }
        $msg = array();
        $msg["status"] = $error_number;
        $msg["status_message"] = "$error_msg";
        $msg["data"] = null;
        return json_encode($msg);
    }

    public static function formatErrorMessage($error_type, $error_msg) {
        $msg = array();
        $msg["error_type"] = $error_type;
        $msg["error_msg"] = $error_msg;
        return json_encode($msg);
    }

    public static function assertNotNull($data, $fields) {


    }

    public static function makeQuery($query_builder) {
        $query="";
        foreach($query_builder as $token) {
            $query=$query." ".$token;
        }
        return $query;
    }
}