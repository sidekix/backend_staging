<?php
/**
 * Created by PhpStorm.
 * User: jerry
 * Date: 1/23/15
 * Time: 8:58 PM
 */
$time_start = microtime(true);

require_once(__DIR__."/../objects/Profile.php");
require_once(__DIR__."/../aux_functions/HelperFuncs.php");

$profile_id = $_GET["id"];
//$access_token = $_GET["access_token"];
try {
    if ($profile_id != null && is_numeric($profile_id) == true) {

        $profile_json = Profile::fetch_complete_profile(intval($profile_id));
        echo HelperFuncs::attachTransactionSuccessHeader($profile_json);

    } else {
        throw new InvalidArgumentException("profile id not specified!");
    }
} catch (Exception $e) {
    echo HelperFuncs::getFailureMessageHeader((string)$e->getMessage(), -1);
}

$time_end = microtime(true);
$execution_time = ($time_end - $time_start);
//echo '<p><b>Total Execution Time:</b> '.$execution_time.'</p>';

function parseAction($requestMessage) {
    $errorMsg = array();
    $decoded = json_decode($requestMessage, true);
    if($decoded == null) {
        throw new  InvalidArgumentException(HelperFuncs::formatErrorMessage("invalid_request", "Cannot decode msg json!"));
    }

    $principal_id = $decoded["principal_id"];
    if($principal_id == null) {
        throw new InvalidArgumentException(HelperFuncs::formatErrorMessage("invalid_request", "principle id not specified"));
    }

    $request = $decoded["request"];
    if($request == null) {
        throw new InvalidArgumentException(HelperFuncs::formatErrorMessage("invalid_request",  "request type not specified"));
    }

    $authentication = $decoded["authentication"];

    $actions = $decoded["actions"];
    if($actions != null || count($actions) > 0) {
        foreach ($actions as $action) {
            $subject = $action["subject"];
            switch($subject) {
                case "profile":
                    //modify profile
                    try {
                        Profile::modifyProfileBasic($action, $authentication);
                    } catch (Exception $e) {
                        array_push($errorMsg, HelperFuncs::formatErrorMessage("invalid_request", $e->getMessage()));
                    }
                    break;
                default:
                    throw new InvalidArgumentException(HelperFuncs::formatErrorMessage("invalid_request",  "invalid subject type: " + $subject));
            }
        }
    }


}

//{"principal_id":"243223", "authentication":{}, "actions":{"action_id":"1" "subject":"profile", "owner_id":"234324", "type":"edit", "edits":{"field":"firstname", "value":"john"}}}
?>
