<?php
/**
 * Created by PhpStorm.
 * User: jerry
 * Date: 1/31/15
 * Time: 12:40 PM
 */

$helper = new \Facebook\FacebookRedirectLoginHelper($redirect_url);
try {
    $session = $helper->getSessionFromRedirect();
} catch(\Facebook\FacebookRequestException $ex) {
    // When Facebook returns an error
} catch(\Exception $ex) {
    // When validation fails or other local issues
}
if ($session) {
    // Logged in.
}