<?php
require_once __DIR__ . '/../../../vendor/autoload.php';
session_start();


$app_id = "881673508521026";
$app_secret = "361ee4a6b9436ff554018fe24cde5b86";


$fb = new Facebook\Facebook([
    'app_id' => $app_id,
    'app_secret' => $app_secret,
    'default_graph_version' => 'v2.2',
]);

$helper = $fb->getRedirectLoginHelper();
$permissions = ['email', 'user_likes', 'public_profile', 'user_about_me', 'user_hometown']; // optional
$loginUrl = $helper->getLoginUrl('http://sidekixapp.com/staging/SideKix/src/www/testing/fb-login-callback.php', $permissions);

echo '<a href="' . $loginUrl . '">Log in with Facebook!</a>';
?>