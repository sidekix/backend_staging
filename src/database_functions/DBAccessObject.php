<?php
require_once "DBConfig.php";
require_once LOGGER;

/**
 * Class DBAccessObject
 */
class DBAccessObject {

    /** Holds the Logger. */
    protected $LOG;

    /** Logger is instantiated in the constructor. */
    public function __construct()
    {
        $this->LOG = new SideKix_LOGGER(__CLASS__);
    }


    /**
     * Connect to a database
     *
     * @author jerry
     * @param String $hostname
     * @param String $username
     * @param String $password
     * @param String $dbname
     * @return a connection
     */
    protected function dbConnect($hostname, $username, $password, $dbname)
    {
        // Create connection
        $con = mysqli_connect($hostname, $username, $password, $dbname);

        // Check connection
        if (mysqli_connect_errno()) {
            $this->LOG->error("Failed to connect to MySQL: " . mysqli_connect_error());
        }
        return $con;
    }

    /**
     * close a connection to a database
     *
     * @author jerry
     * @param unknown $con
     */
    protected function dbClose($con)
    {
        mysqli_close($con);
    }

}
?>