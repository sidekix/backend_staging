<?php

require_once(__DIR__ . "/../DBConfig.php");

class LocationBasedCalc {

  static function distanceGeo ($location_1, $location_2) {
    if($location_1 == null || gettype($location_1) != $GLOBALS["TABLE_PROFILES_SCHEMA_TYPE"]["last_geotag"])
    {
      throw new UnexpectedValueException("invalid coordinates in first parameter");
    }
    if($location_2 == null || gettype($location_2) != $GLOBALS["TABLE_PROFILES_SCHEMA_TYPE"]["last_geotag"])
    {
      throw new UnexpectedValueException("invalid coordinates in second parameter");
    }

    $coordinates1 = explode(",", $location_1);
    $coordinates2 = explode(",", $location_2);

    $lat1 = floatval($coordinates1[0]);
    $lon1 = floatval($coordinates1[1]);

    $lat2 = floatval($coordinates2[0]);
    $lon2 = floatval($coordinates2[1]);

    return self::distance($lat1, $lon1, $lat2, $lon2, DISTANCE_UNIT);

  }

  static function distance($lat1, $lon1, $lat2, $lon2, $unit) {
    if(gettype($lat1)!="double" || gettype($lon1)!="double" || gettype($lon1)!="double" || gettype($lon1)!="double")
    {
      throw new UnexpectedValueException("invalid type in latitude and longitude coordinates");
    }

    $theta = $lon1 - $lon2;
    $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
    $dist = acos($dist);
    $dist = rad2deg($dist);
    $miles = $dist * 60 * 1.1515;
    $unit = strtoupper($unit);

    if ($unit == KILOMETER) {
      return ($miles * 1.609344);
    } else if ($unit == NAUTICAL_MILE) {
      return ($miles * 0.8684);
    } else {
      return $miles;
    }
  }
}

?>
