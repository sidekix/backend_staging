<?php
/**
 * Created by PhpStorm.
 * User: jerry
 * Date: 1/21/15
 * Time: 12:33 PM
 */

require_once('DBConfig.php');
require_once("SideKixDBAccessObject.php");
require_once(__DIR__ . "/../aux_functions/HelperFuncs.php");

class UserAuthenticationDAO extends SideKixDBAccessObject
{
    private $authenticated = false;
    private $TOKEN_LENGTH = 512;

    public function __construct()
    {
        parent::__construct();
        $this->LOG = new SideKix_LOGGER(__CLASS__);
    }

   /*
    * NOT Done
    */
    public function authenticateCredentialsWithFB($fbid, $token)
    {
        HelperFuncs::checkType($fbid, STR, __CLASS__, __METHOD__);
        HelperFuncs::checkType($token, STR, __CLASS__, __METHOD__);

        $query = "SELECT * FROM " . TABLE_PROFILES . " WHERE " . $GLOBALS ["TABLE_PROFILES_SCHEMA"] ["fbid"] . "=" . "'" . $fbid . "'";

        $result = mysqli_query($this->con, $query);

        if (!$result) {

            $this->LOG->error("Cannot retrieve profile with fbid " . $fbid . " from db " . mysqli_error($this->con));
            return null;
        }

        if (mysqli_num_rows($result) == 0) {
            $this->LOG->warning("no results for profile with fbid: " . $fbid);
            return null;
        } else if (mysqli_num_rows($result) > 1) {
            $this->LOG->error("more than one results for profile with fbid: " . $fbid);
            exit (-1);
        }

        $result_row = mysqli_fetch_array($result);

    }

    public function authenticateCredentialsWithSidkixDB($username, $password)
    {
        HelperFuncs::checkType($username, STR, __CLASS__, __METHOD__);
        HelperFuncs::checkType($username, STR, __CLASS__, __METHOD__);

        $query = "SELECT * FROM " . TABLE_PROFILES . " WHERE " . $GLOBALS ["TABLE_PROFILES_SCHEMA"] ["username"] . "=" . "'" . $username . "'";

        $result = mysqli_query($this->con, $query);

        if (!$result) {

            $this->LOG->error("Cannot retrieve profile " . $username . " from db " . mysqli_error($this->con));
            return null;
        }

        if (mysqli_num_rows($result) == 0) {
            $this->LOG->warning("no results for profile username: " . $username);
            return null;
        } else if (mysqli_num_rows($result) > 1) {
            $this->LOG->error("more than one results for profile username: " . $username);
            exit (-1);
        }

        $result_row = mysqli_fetch_array($result);


        $hashed_password = hash("sha512", $password, false);
        $this->LOG->debug("hashed_password: " . $hashed_password);
        $this->LOG->debug("db password: " . $result_row[$GLOBALS ["TABLE_PROFILES_SCHEMA"] ["password"]]);
        if ($hashed_password == $result_row[$GLOBALS ["TABLE_PROFILES_SCHEMA"] ["password"]]) {
            $this->LOG->debug("user: " . $username . " successfully authenticated");
            $this->authenticated = true;
            return $this->getAccessToken($username);
        }

        return null;
    }

    public function authenticateWithToken($username, $token)
    {
        $query = "SELECT * FROM " . TABLE_OATH_ACCESS_TOKENS . " WHERE " . $GLOBALS ["TABLE_OATH_ACCESS_TOKENS_SCHEMA"] ["username"] . "=" . "'" . $username . "' AND " . $GLOBALS ["TABLE_OATH_ACCESS_TOKENS_SCHEMA"] ["oath_access_token"]."="."'" .$token."'";

        $this->LOG->debug("query: ". $query);
        $result = mysqli_query($this->con, $query);

        if (!$result) {

            $this->LOG->error("Cannot retrieve profile " . $this->username . " from db " . mysqli_error($this->con));
            return false;
        }

        if (mysqli_num_rows($result) == 0) {
            $this->LOG->warning("no results for profile username: " . $username);
            return false;
        }

        $result_row = mysqli_fetch_array($result);
        $expiration_time = intval($result_row[$GLOBALS ["TABLE_OATH_ACCESS_TOKENS_SCHEMA"] ["expiration_time"]]);
        $date = new DateTime();
        $current_time = $date->getTimestamp();

        if($expiration_time<$current_time) {
            return false;
        }

        $this->LOG->debug("authentication successful using access token for user: ".$username);
        return true;
    }

    private function getAccessToken($username)
    {
        if ($this->authenticated == false) {
            return null;
        }

        $token = $this->generateAccessToken($username);
        $this->insertTokenIntoDb($username, $token);
        return $token;

    }

    private function insertTokenIntoDb($username, $token)
    {
        $date = new DateTime();
        $create_time = $date->getTimestamp();
        $expiration_time = $create_time+TOKEN_VALID_DURATION;

        $sql = "INSERT INTO " . TABLE_OATH_ACCESS_TOKENS . " (username, oath_access_token, create_time, expiration_time) VALUES ('" . $username . "','" . $token . "','" . $create_time . "','" . $expiration_time . "')";

        $this->LOG->debug("query: " . $sql);

        if ($this->con->query($sql) === TRUE) {
            $this->LOG->debug("New record created successfully");
        } else {
            $this->LOG->debug("Error: " . $sql . "<br>" . $this->con->error);
        }
    }

    private function generateAccessToken()
    {
        return $this->getToken($this->TOKEN_LENGTH);
    }

    private function crypto_rand_secure($min, $max)
    {
        $range = $max - $min;
        if ($range < 0) return $min; // not so random...
        $log = log($range, 2);
        $bytes = (int)($log / 8) + 1; // length in bytes
        $bits = (int)$log + 1; // length in bits
        $filter = (int)(1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd >= $range);
        return $min + $rnd;
    }

    private function getToken($length)
    {
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet .= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet .= "0123456789";
        for ($i = 0; $i < $length; $i++) {
            $token .= $codeAlphabet[$this->crypto_rand_secure(0, strlen($codeAlphabet))];
        }
        return $token;
    }


}