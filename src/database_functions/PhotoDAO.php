<?php
/**
 * Created by PhpStorm.
 * User: jerry
 * Date: 1/24/15
 * Time: 8:09 AM
 */

require_once("SideKixDBAccessObject.php");

class PhotoDAO extends SideKixDBAccessObject{

    public function __construct()
    {
        parent::__construct(TABLE_PHOTOS);
        $this->LOG = new SideKix_LOGGER(__CLASS__);
    }

    public function getPhotoWithId($photo_id)
    {
        if (gettype($photo_id) != $GLOBALS ["TABLE_PHOTOS_SCHEMA_TYPE"]["id"]) {
            throw new InvalidArgumentException("photo_id type incorrect");
        }
        $fields = array(
            "id" => $photo_id
        );
        $results =  $this->getObjectWithFields($fields);
        if (sizeof($results) == 0) {
            $this->LOG->warning("no results for photo_id : " . $photo_id);
        } elseif (sizeof($results) != 1) {
            $this->LOG->error("More than one photo has photo id: " . $photo_id);
            throw new UnexpectedValueException("More than one photo has photo id: " . $photo_id);
        }
        return $results[0];
    }

}