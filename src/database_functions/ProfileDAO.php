<?php
/**
 * Created by PhpStorm.
 * User: jerry
 * Date: 1/23/15
 * Time: 8:59 PM
 */

require_once("SkillDAO.php");
require_once("PhotoDAO.php");
require_once("ReviewDAO.php");
require_once("ProfileSkillDAO.php");
require_once("SkillPhotoDAO.php");
require_once("ProfileSkillReviewDAO.php");

class ProfileDAO extends SideKixDBAccessObject
{

    public function __construct()
    {
        parent::__construct(TABLE_PROFILES);
        $this->LOG = new SideKix_LOGGER(__CLASS__);
    }

    /**
     * Fetches the data of a given profile.
     *
     * @author jerry
     * @param unknown $profile_id
     * @return NULL|unknown
     */


    public function getProfileWithId($profile_id)
    {
        if (gettype($profile_id) != $GLOBALS ["TABLE_PROFILES_SCHEMA_TYPE"]["id"]) {
            throw new InvalidArgumentException("profile_id type incorrect");
        }
        $fields = array(
            "id" => $profile_id
        );
        $results = $this->getObjectWithFields($fields);
        if (sizeof($results) == 0) {
            $this->LOG->warning("no results for profile_id : " . $profile_id);
        } elseif (sizeof($results) != 1) {
            $this->LOG->error("More than one profile has profile id: " . $profile_id);
            throw new UnexpectedValueException("More than one profile has profile id: " . $profile_id);
        }
        return $results[0];
    }

    /**
     * fetches all the skills a profile has.
     *
     * @author jerry
     * @param unknown $profile_id
     * @return NULL|multitype:
     */
    public function fetchProfileSkills($profile_id, $start = 0, $end = PROFILE_SKILLS_ENTRY_LIMIT)
    {
        if (gettype($profile_id) != $GLOBALS ["TABLE_R_PROFILE_SKILLS_SCHEMA_TYPE"]["profile_id"]) {
            throw new InvalidArgumentException("profile_id type incorrect");
        }

        $skills = array();
        $offset = $end - $start;
        $query = "SELECT skills.*, R_profile_skills.id AS profile_skill_id, R_profile_skills.description FROM R_profile_skills JOIN skills ON R_profile_skills.skill_id=skills.id where R_profile_skills.profile_id='".$profile_id."' LIMIT " . $offset . " OFFSET " . $start;
        $this->LOG->debug("Query: ".$query);

        $skill_result = mysqli_query($this->con, $query);

        if (!$skill_result) {
            $this->LOG->error("Cannot retrieve skills for profile: ".$profile_id." ". mysqli_error($this->con));
            return null;
        }
        if (mysqli_num_rows($skill_result) == 0) {
            $this->LOG->warning("no results for profile id: " . $profile_id);
            return null;
        }

        while ($result_row = mysqli_fetch_array($skill_result, MYSQLI_ASSOC)) {
            array_push($skills, $result_row);
        }

        return $skills;
    }

    /**
     * fetches all the photos user uploaded for a skill he/she has.
     *
     * @author jerry
     * @param unknown $profile_id
     * @param unknown $skill_id
     * @param int $start
     * @param int $end
     * @return array|null
     */

    public function fetchSkillPhotos($profile_skill_id, $start = 0, $end = SKILL_PHOTOS_ENTRY_LIMIT)
    {
        if (gettype($profile_skill_id) != $GLOBALS ["TABLE_R_SKILL_PHOTOS_SCHEMA_TYPE"]["profile_skill_id"]) {
            throw new InvalidArgumentException("profile_skill_id type incorrect");
        }

        $photos = array();
        $offset = $end - $start;

        $query = "SELECT photos.* FROM R_skill_photos JOIN photos ON R_skill_photos.photo_id=photos.id where R_skill_photos.profile_skill_id='".$profile_skill_id."' LIMIT " . $offset . " OFFSET " . $start;
        $this->LOG->debug("Query: ".$query);

        $photo_result = mysqli_query($this->con, $query);

        if (!$photo_result) {
            $this->LOG->error("Cannot retrieve Photos for profile_skill_id: ".$profile_skill_id." ". mysqli_error($this->con));
            return null;
        }
        if (mysqli_num_rows($photo_result) == 0) {
            $this->LOG->warning("no results for profile skill id: " . $profile_skill_id);
            return null;
        }

        while ($result_row = mysqli_fetch_array($photo_result, MYSQLI_ASSOC)) {
            array_push($photos, $result_row);
        }
        return $photos;
    }

    /**
     * fetches all reviews for a skill within a start id and end id
     *
     * @author jerry
     * @param unknown $profile_skill_id
     * @param $start is the start id of reviews
     * @param $end is the end id of reviews
     */

    function fetchSkillReviews($profile_skill_id, $start = 0, $end = SKILL_REVIEWS_ENTRY_LIMIT)
    {
        if (gettype($profile_skill_id) != $GLOBALS ["TABLE_R_PROFILE_SKILL_REVIEWS_SCHEMA_TYPE"]["profile_skill_id"]) {
            throw new InvalidArgumentException("profile_skill_id type incorrect");
        }

        $reviews = array();
        $offset = $end - $start;

        $query = "SELECT reviews.* FROM R_profile_skill_reviews JOIN reviews ON R_profile_skill_reviews.review_id=reviews.id where R_profile_skill_reviews.profile_skill_id='".$profile_skill_id."' LIMIT " . $offset . " OFFSET " . $start;
        $this->LOG->debug("Query: ".$query);

        $reviews_result = mysqli_query($this->con, $query);

        if (!$reviews_result) {
            $this->LOG->error("Cannot retrieve reviews for profile_skill_id: ".$profile_skill_id." ". mysqli_error($this->con));
            return null;
        }
        if (mysqli_num_rows($reviews_result) == 0) {
            $this->LOG->warning("no results for profile skill id: " . $profile_skill_id);
            return null;
        }

        while ($result_row = mysqli_fetch_array($reviews_result, MYSQLI_ASSOC)) {
            array_push($reviews, $result_row);
        }
        return $reviews;
    }

    /**
     * fetches the geotag for a profile
     *
     * @param $profile_id
     */
    function fetchGeoTag($profile_id)
    {
        if (gettype($profile_id) != $GLOBALS["TABLE_PROFILES_SCHEMA_TYPE"]["id"]) {
            throw new InvalidArgumentException("profile_id type incorrect");
        }

        $profile_data = $this->fetchProfile($profile_id);
        return $profile_data[$GLOBALS ["TABLE_PROFILES_SCHEMA"]["last_geotag"]];
    }

    function modifyProfileBasic($profile_id, $updates) {

        $query_builder = array();
        array_push($query_builder, "UPDATE");
        array_push($query_builder, TABLE_PROFILES);
        array_push($query_builder, "SET");
        foreach ($updates as $update) {
            $field = $update["field"];
            $value = $update["value"];
            $change = $field."=".$value;
            array_push($query_builder, $change);
        }
        array_push($query_builder, "WHERE");
        array_push($query_builder, $GLOBALS["TABLE_PROFILES_SCHEMA_TYPE"]["id"]."=".$profile_id);
        $query = HelperFuncs::makeQuery($query_builder);
        if(!mysqli_query($this->con, $query)) {
            $this->LOG->error("Could not update profile entries: ".$updates." for profile: ".$profile_id." - ".mysqli_error($this->con));
            throw new UnexpectedValueException("Could not update profile entries: ".$updates." for profile: ".$profile_id);
        }
        mysqli_close($this->con);
    }
}