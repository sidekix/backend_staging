<?php
/**
 * Created by PhpStorm.
 * User: jerry
 * Date: 1/25/15
 * Time: 2:30 PM
 */

require_once("SideKixDBAccessObject.php");

class ProfileSkillDAO extends SideKixDBAccessObject{

    public function __construct()
    {
        parent::__construct(TABLE_R_PROFILE_SKILLS);
        $this->LOG = new SideKix_LOGGER(__CLASS__);
    }
}