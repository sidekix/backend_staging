<?php
/**
 * Created by PhpStorm.
 * User: jerry
 * Date: 1/24/15
 * Time: 7:07 AM
 */

require_once("SideKixDBAccessObject.php");

class SkillDAO extends SideKixDBAccessObject{

    public function __construct()
    {
        parent::__construct(TABLE_SKILLS);
        $this->LOG = new SideKix_LOGGER(__CLASS__);
    }

    public function getSkillWithId($skill_id)
    {
        if (gettype($skill_id) != $GLOBALS ["TABLE_SKILLS_SCHEMA_TYPE"]["id"]) {
            throw new InvalidArgumentException("skill_id type incorrect");
        }
        $fields = array(
            "id" => $skill_id
        );
        $results =  $this->getObjectWithFields($fields);
        if (sizeof($results) == 0) {
            $this->LOG->warning("no results for skill_id : " . $skill_id);
        } elseif (sizeof($results) != 1) {
            $this->LOG->error("More than one skill has skill id: " . $skill_id);
            throw new UnexpectedValueException("More than one skill has skill id: " . $skill_id);
        }
        return $results[0];
    }

}
?>