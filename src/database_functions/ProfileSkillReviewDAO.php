<?php
/**
 * Created by PhpStorm.
 * User: jerry
 * Date: 1/25/15
 * Time: 4:15 PM
 */

require_once("SideKixDBAccessObject.php");

class ProfileSkillReviewDAO extends SideKixDBAccessObject{

    public function __construct()
    {
        parent::__construct(TABLE_R_PROFILE_SKILL_REVIEWS);
        $this->LOG = new SideKix_LOGGER(__CLASS__);
    }
}