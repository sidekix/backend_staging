<?php

require_once('DBConfig.php');
require_once('DBAccessObject.php');
require_once LOGGER;

/**
 * Class SideKixDBAccessObject
 *
 * Object that facilitates data reads from SideKix DB
 *
 * @author jerry
 *
 */
abstract class SideKixDBAccessObject extends DBAccessObject
{

    /**
     * @var variable that stores a connection to DB
     */
    protected $con = null;

    protected $table = null;

    protected $LOG=null;

    /**
     * constructor that initializes a connection to the SideKix DB
     *
     * @author jerry
     */
    public function __construct($table)
    {
        parent::__construct();
        $this->LOG = new SideKix_LOGGER(__CLASS__);
        //connect to SideKix DB
        $this->con = $this->dbConnectSidekix();
        $this->table = $table;
    }

    /**
     * destructor that terminates a connection to the SideKix DB
     *
     * @author jerry
     */
    function __destruct()
    {
        $this->dbClose($this->con);
    }

    /**
     * connect to a sidekix db
     *
     * @author jerry
     * @return unknown
     */
    private function dbConnectSidekix()
    {
        $con = $this->dbConnect(HOSTNAME, USERNAME, PASSWORD, DBNAME);
        return $con;
    }

    public function getObjectWithFields($fields, $start=0, $end=MAX_ENTRY_LIMIT)
    {
        //$this->LOG->debug("start: ".$start." end: ".$end." table: ".$this->table);

        $query = "SELECT * FROM " . $this->table . " WHERE ";

        foreach ($fields as $key => $val) {
            $query = $query . $key . "='" . $val . "' ";
        }

        $offset = $end - $start;
        $query = $query . " LIMIT " . $offset . " OFFSET " . $start;

        $this->LOG->debug("Query: ".$query);

        $result = mysqli_query($this->con, $query);
        if (!$result) {
            $this->LOG->error("Cannot retrieve " . $this->table . " with fields: " . json_encode($fields) . " from db " . mysqli_error($this->con));
            return null;
        }

        $retVal = array();
        while ($result_row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            foreach ($fields as $key => $val) {
                array_push($retVal, $result_row);
            }
        }

        return $retVal;

    }
}

?>