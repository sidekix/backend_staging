<?php
/**
 * Created by PhpStorm.
 * User: jerry
 * Date: 1/25/15
 * Time: 2:28 PM
 */

require_once("SideKixDBAccessObject.php");

class SkillPhotoDAO extends SideKixDBAccessObject{

    public function __construct()
    {
        parent::__construct(TABLE_R_SKILL_PHOTOS);
        $this->LOG = new SideKix_LOGGER(__CLASS__);
    }
}