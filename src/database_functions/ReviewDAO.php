<?php
/**
 * Created by PhpStorm.
 * User: jerry
 * Date: 1/23/15
 * Time: 8:29 PM
 */

require_once("SideKixDBAccessObject.php");

class ReviewDAO extends SideKixDBAccessObject {

    public function __construct()
    {
        parent::__construct(TABLE_REVIEWS);
        $this->LOG = new SideKix_LOGGER(__CLASS__);
    }

    public function getReviewWithId($review_id)
    {
        if (gettype($review_id) != $GLOBALS ["TABLE_REVIEWS_SCHEMA_TYPE"]["id"]) {
            throw new InvalidArgumentException("review_id type incorrect");
        }
        $fields = array(
            "id" => $review_id
        );
        $results =  $this->getObjectWithFields($fields);
        if (sizeof($results) == 0) {
            $this->LOG->warning("no results for review_id : " . $review_id);
        } elseif (sizeof($results) != 1) {
            $this->LOG->error("More than one review has review id: " . $review_id);
            throw new UnexpectedValueException("More than one review has review id: " . $review_id);
        }
        return $results[0];
    }
}