<?php
/**
 * DBConfig.php
 * @author jerry
 */

/**
 * includes
 */

require_once __DIR__."/../Config.php";

/**
 * Globals
 */
define ( "HOSTNAME", "sidekixapp.com" );
define ( "USERNAME", "sidekixstaging" );
define ( "DBNAME", "sidekixstaging" );
define ( "PASSWORD", "ftskMatw10!" );
define ( "TABLE_PROFILES", "profiles" );
define ( "TABLE_R_PROFILE_SKILLS", "R_profile_skills" );
define ( "TABLE_SKILLS", "skills" );
define ( "TABLE_PHOTOS", "photos");
define ("TABLE_R_SKILL_PHOTOS", "R_skill_photos");
define ("TABLE_REVIEWS", "reviews");
define ("TABLE_OATH_ACCESS_TOKENS", "oath_access_tokens");
define ("TABLE_R_PROFILE_SKILL_REVIEWS", "R_profile_skill_reviews");
define ("TABLE_R_PROFILE_REVIEWS", "R_profile_reviews");

/**
 * Defines database schema
 */
$GLOBALS ["TABLE_PROFILES_SCHEMA"] = array (
		"id" => "id",
		"username" => "username",
		"password" => "password",
		"fbid" => "fbid",
		"first_name" => "first_name",
		"last_name" => "last_name",
		"create_time" => "create_time",
		"last_login_time" => "last_login_time",
		"admin" => "admin",
		"photo_id" => "photo_id",
		"bio" => "bio",
		"html_link" => "html_link",
		"last_geotag" => "last_geotag" 
);
$GLOBALS ["TABLE_SKILLS_SCHEMA"] = array (
		"id" => "id",
		"name" => "name",
		"is_predefined" => "is_predefined",
		"time_created" => "time_created",
		"creator_profile_id" => "creator_profile_id" 
);

$GLOBALS ["TABLE_R_PROFILE_SKILLS_SCHEMA"] = array (
		"id" => "id",
		"profile_id" => "profile_id",
		"skill_id" => "skill_id",
		"description" => "description",
		"retired" => "retired" 
);

$GLOBALS ["TABLE_PHOTOS_SCHEMA"] = array (
		"id" => "id",
		"profile_id" => "profile_id",
		"upload_time" => "upload_time",
		"file_path" => "file_path",
		"geotag" => "geotag",
		"views" => "views",
		"likes" => "likes"
);

$GLOBALS ["TABLE_R_SKILL_PHOTOS_SCHEMA"] = array (
		"id" => "id",
		"profile_skill_id" => "profile_skill_id",
		"photo_id" => "photo_id"
);

$GLOBALS ["TABLE_R_PROFILE_SKILL_REVIEWS_SCHEMA"] = array (
	"id" => "id",
	"profile_skill_id" => "profile_skill_id",
	"review_id" => "review_id"
);

$GLOBALS ["TABLE_R_PROFILE_REVIEWS_SCHEMA"] = array (
	"id" => "id",
	"profile_id" => "profile_id",
	"review_id" => "review_id"
);

$GLOBALS ["TABLE_REVIEWS_SCHEMA"] = array (
		"id" => "id",
		"reviewer_profile_id" => "reviewer_profile_id",
		"time" => "time",
		"stars" => "stars",
		"review" => "review"
);

$GLOBALS ["TABLE_OATH_ACCESS_TOKENS_SCHEMA"] = array (
	"id" => "id",
	"username" => "username",
	"oath_access_token" => "oath_access_token",
	"create_time" => "create_time",
	"expiration_time" => "expiration_time"
);

/*
 * Defines data type for table fields
 */
$GLOBALS["TABLE_PROFILES_SCHEMA_TYPE"] = array(
	$GLOBALS ["TABLE_PROFILES_SCHEMA"] ["id"] => "integer",
	$GLOBALS ["TABLE_PROFILES_SCHEMA"] ["last_geotag"] => "string"
);

$GLOBALS ["TABLE_R_PROFILE_SKILLS_SCHEMA_TYPE"] = array(
	$GLOBALS ["TABLE_R_PROFILE_SKILLS_SCHEMA"] ["id"] => "integer",
	$GLOBALS ["TABLE_R_PROFILE_SKILLS_SCHEMA"] ["profile_id"] => "integer"

);

$GLOBALS ["TABLE_R_SKILL_PHOTOS_SCHEMA_TYPE"] = array(
	$GLOBALS ["TABLE_R_SKILL_PHOTOS_SCHEMA"] ["id"] => "integer",
	$GLOBALS ["TABLE_R_SKILL_PHOTOS_SCHEMA"] ["profile_skill_id"] => "integer",
	$GLOBALS ["TABLE_R_SKILL_PHOTOS_SCHEMA"] ["photo_id"] => "integer",
);

$GLOBALS ["TABLE_REVIEWS_SCHEMA_TYPE"] = array (
	$GLOBALS ["TABLE_REVIEWS_SCHEMA"]["id"] => "integer",
	$GLOBALS ["TABLE_REVIEWS_SCHEMA"]["reviewer_profile_id"] => "integer",
	$GLOBALS ["TABLE_REVIEWS_SCHEMA"]["time"] => "integer",
	$GLOBALS ["TABLE_REVIEWS_SCHEMA"]["stars"] => "integer",
	$GLOBALS ["TABLE_REVIEWS_SCHEMA"]["review"] => "string"
);

$GLOBALS ["TABLE_PHOTOS_SCHEMA_TYPE"] = array (
	$GLOBALS ["TABLE_PHOTOS_SCHEMA"]["id"] => "integer"
);

$GLOBALS ["TABLE_SKILLS_SCHEMA_TYPE"] = array (
	$GLOBALS ["TABLE_SKILLS_SCHEMA"]["id"] => "integer"
);

$GLOBALS ["TABLE_R_PROFILE_SKILL_REVIEWS_SCHEMA_TYPE"] = array(
	$GLOBALS ["TABLE_R_PROFILE_SKILL_REVIEWS_SCHEMA"] ["id"] => "integer",
	$GLOBALS ["TABLE_R_PROFILE_SKILL_REVIEWS_SCHEMA"] ["profile_skill_id"] => "integer",
	$GLOBALS ["TABLE_R_PROFILE_SKILL_REVIEWS_SCHEMA"] ["review_id"] => "integer"


);

/**
 * units used
 */
define (	"MILE", "MILE" );
define ("KILOMETER", "KILOMETER");
define ("NAUTICAL_MILE", "NAUTICAL_MILE");
define ( "DISTANCE_UNIT", MILE );

/**
 * Entry retrieval limit
 */

define("SKILL_REVIEWS_ENTRY_LIMIT", 5);
define("PROFILE_SKILLS_ENTRY_LIMIT", 5);
define("SKILL_PHOTOS_ENTRY_LIMIT", 5);
define ("MAX_ENTRY_LIMIT", 100);

/**
 * OATH access token valid duration
 */

define ("TOKEN_VALID_DURATION", 86400);

?>
