<?php
/**
 * Created by PhpStorm.
 * User: jerry
 * Date: 1/24/15
 * Time: 7:21 AM
 */

require_once __DIR__ . "/../src/Config.php";
require_once __DIR__ . "/../src/objects/Review.php";

class ReviewTest extends PHPUnit_Framework_TestCase {

    protected static $LOG;

    /*
     * Setup before test is run
     */
    public static function setUpBeforeClass()
    {
        self::$LOG = new SideKix_LOGGER(__CLASS__);
    }

    /*
     * tear down after tests are complete
     */
    public static function tearDownAfterClass()
    {
        //stub
    }

    public function test_fetchReview()
    {
        self::$LOG->debug("/** Entering " . __METHOD__ . " **/");

        $results = Review::getReviewWithId(1);
        self::$LOG->debug("results: " . $results);
        $this->assertEquals("1", $results->get_id());
        $this->assertEquals("1", $results->get_reviewer_profile_id());
        $this->assertEquals("1420615395", $results->get_time());
        $this->assertEquals("1", $results->get_stars());
        $this->assertEquals("He sucks really bad at this", $results->get_review());

        self::$LOG->debug("/** Exiting " . __METHOD__ . " **/");

    }
}
