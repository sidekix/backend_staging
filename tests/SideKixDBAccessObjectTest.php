<?php
//
///**
// * User: jerry
// * Date: 1/10/15
// * Time: 12:36 AM
// */
//
//require_once __DIR__ . "/../src/Config.php";
//require_once __DIR__ . "/../src/database_functions/SideKixDBAccessObject.php";
//
//class SideKixDBAccessObjectTest extends PHPUnit_Framework_TestCase
//{
//
//    protected static $LOG;
//    protected static $sideKixDAO;
//
//    /*
//     * Setup before test is run
//     */
//    public static function setUpBeforeClass()
//    {
//        self::$LOG = new SideKix_LOGGER(__CLASS__);
//        self::$sideKixDAO = new SideKixDBAccessObject();
//    }
//
//    /*
//     * tear down after tests are complete
//     */
//    public static function tearDownAfterClass()
//    {
//        //stub
//    }
//
//    public function test_fetchProfile()
//    {
//        /*
//         *  results: {"0":"2","id":"2","1":"user_b","username":"user_b","2":"pw",
//         * "password":"pw","3":"2","fbid":"2","4":"Sea",
//         * "first_name":"Sea","5":"Pong","last_name":"Pong","6":"1420615390",
//         * "create_time":"1420615390","7":"1420615400","last_login_time":"1420615400","8":"0",
//         * "admin":"0","9":"2","photo_id":"2","10":"Bio for Sea Pong - he is a dick!",
//         * "bio":"Bio for Sea Pong - he is a dick!","11":"www.seapong.com",
//         * "html_link":"www.seapong.com","12":"34.0500,118.2500","last_geotag":"34.0500,118.2500"}
//         */
//        self::$LOG->debug("/** Entering " . __METHOD__ . " **/");
//
//        $results = self::$sideKixDAO->fetchProfile(2);
//        self::$LOG->debug("results: " . json_encode($results));
//        $this->assertEquals("Sea", $results["first_name"]);
//        $this->assertEquals("Pong", $results["last_name"]);
//        $this->assertEquals("2", $results["id"]);
//        $this->assertEquals("user_b", $results["username"]);
//        $this->assertEquals("pw", $results["password"]);
//        $this->assertEquals("2", $results["fbid"]);
//        $this->assertEquals("1420615390", $results["create_time"]);
//        $this->assertEquals("1420615400", $results["last_login_time"]);
//        $this->assertEquals("0", $results["admin"]);
//        $this->assertEquals("2", $results["photo_id"]);
//        $this->assertEquals("Bio for Sea Pong - he is a dick!", $results["bio"]);
//        $this->assertEquals("www.seapong.com", $results["html_link"]);
//        $this->assertEquals("34.0500,118.2500", $results["last_geotag"]);
//
//        self::$LOG->debug("/** Exiting " . __METHOD__ . " **/");
//
//    }
//
//    public function test_fetchSkill()
//    {
//        /*
//         *results: {"0":"2","id":"2","1":"Washing","name":"Washing","2":"1","is_predefined":"1","3":"123232",
//         * "time_created":"123232","4":"1","creator_profile_id":"1"}
//         */
//        self::$LOG->debug("/** Entering " . __METHOD__ . " **/");
//
//        $results = self::$sideKixDAO->fetchSkill(2);
//        self::$LOG->debug("results: " . json_encode($results));
//        $this->assertEquals("2", $results["id"]);
//        $this->assertEquals("Washing", $results["name"]);
//        $this->assertEquals("1", $results["is_predefined"]);
//        $this->assertEquals("1", $results["creator_profile_id"]);
//
//        self::$LOG->debug("/** Exiting " . __METHOD__ . " **/");
//
//    }
//
//    public function test_fetchPhoto()
//    {
//        /*
//         * results: {"0":"2","id":"2","1":"2","profile_id":"2","2":"1420615390","upload_time":"1420615390",
//         * "3":"\/data\/fakePath\/profile_2.jpg","file_path":"\/data\/fakePath\/profile_2.jpg",
//         * "4":"34.0500,118.2500","geotag":"34.0500,118.2500","5":"122","views":"122","6":"232","likes":"232"}
//         */
//        self::$LOG->debug("/** Entering " . __METHOD__ . " **/");
//
//        $results = self::$sideKixDAO->fetchPhoto(2);
//        self::$LOG->debug("results: ". json_encode($results));
//        $this->assertEquals("2", $results["id"]);
//        $this->assertEquals("2", $results["profile_id"]);
//        $this->assertEquals("/data/fakePath/profile_2.jpg", $results["file_path"]);
//        $this->assertEquals("34.0500,118.2500", $results["geotag"]);
//        $this->assertEquals("122", $results["views"]);
//        $this->assertEquals("232", $results["likes"]);
//
//        self::$LOG->debug("/** Exiting " . __METHOD__ . " **/");
//
//    }
//
//    public function test_fetchSkillReviews()
//    {
//        self::$LOG->debug("/** Entering " . __METHOD__ . " **/");
//        $profileSkillId = 2;
//        $reviews = self::$sideKixDAO->fetchSkillReviews($profileSkillId, 0, 1000);
//        $totalSize = sizeof($reviews);
//
//        self::$LOG->debug("Total number of reviews: " . $totalSize);
//        $startId = 0;
//        $endId = SKILL_REVIEWS_ENTRY_LIMIT;
//        while ($totalSize > 0) {
//
//            $reviews = self::$sideKixDAO->fetchSkillReviews($profileSkillId, $startId, $endId);
//            self::$LOG->debug("totalSize: " . $totalSize . " # of reviews returned: " . sizeof($reviews) . " startId: " . $startId . " endId: " . $endId);
//
//            if ($totalSize > SKILL_REVIEWS_ENTRY_LIMIT) {
//                $this->assertEquals(SKILL_REVIEWS_ENTRY_LIMIT, sizeof($reviews));
//            } else {
//                $this->assertEquals($totalSize, sizeof($reviews));
//            }
//            $totalSize = $totalSize - SKILL_REVIEWS_ENTRY_LIMIT;
//            $startId = $startId + SKILL_REVIEWS_ENTRY_LIMIT;
//            $endId = $endId + SKILL_REVIEWS_ENTRY_LIMIT;
//        }
//        self::$LOG->debug("/** Exiting " . __METHOD__ . " **/");
//    }
//
//    public function test_fetchProfileSkills()
//    {
//        self::$LOG->debug("/** Entering " . __METHOD__ . " **/");
//
//        $profileSkillId = 2;
//        $reviews = self::$sideKixDAO->fetchProfileSkills($profileSkillId, 0, 1000);
//        self::$LOG->debug("reviews: ".json_encode($reviews));
//        $totalSize = sizeof($reviews);
//
//        self::$LOG->debug("Total number of Skills: " . $totalSize);
//        $startId = 0;
//        $endId = PROFILE_SKILLS_ENTRY_LIMIT;
//        while ($totalSize > 0) {
//
//            $reviews = self::$sideKixDAO->fetchProfileSkills($profileSkillId, $startId, $endId);
//            self::$LOG->debug("totalSize: " . $totalSize . " # of Skills returned: " . sizeof($reviews) . " startId: " . $startId . " endId: " . $endId);
//
//            if ($totalSize > PROFILE_SKILLS_ENTRY_LIMIT) {
//                $this->assertEquals(PROFILE_SKILLS_ENTRY_LIMIT, sizeof($reviews));
//            } else {
//                $this->assertEquals($totalSize, sizeof($reviews));
//            }
//            $totalSize = $totalSize - PROFILE_SKILLS_ENTRY_LIMIT;
//            $startId = $startId + PROFILE_SKILLS_ENTRY_LIMIT;
//            $endId = $endId + PROFILE_SKILLS_ENTRY_LIMIT;
//        }
//
//
//        self::$LOG->debug("/** Exiting " . __METHOD__ . " **/");
//    }
//
//    public function test_fetchSkillPhotos()
//    {
//        self::$LOG->debug("/** Entering " . __METHOD__ . " **/");
//
//        $profile_id = 3;
//        $skill_id = 2;
//        $reviews = self::$sideKixDAO->fetchSkillPhotos($profile_id, $skill_id, 0, 1000);
//        $totalSize = sizeof($reviews);
//
//        self::$LOG->debug("Total number of photos: " . $totalSize);
//        $startId = 0;
//        $endId = SKILL_PHOTOS_ENTRY_LIMIT;
//        while ($totalSize > 0) {
//
//            $reviews = self::$sideKixDAO->fetchSkillPhotos($profile_id, $skill_id, $startId, $endId);
//            self::$LOG->debug("totalSize: " . $totalSize . " # of photos returned: " . sizeof($reviews) . " startId: " . $startId . " endId: " . $endId);
//
//            if ($totalSize > SKILL_PHOTOS_ENTRY_LIMIT) {
//                $this->assertEquals(SKILL_PHOTOS_ENTRY_LIMIT, sizeof($reviews));
//            } else {
//                $this->assertEquals($totalSize, sizeof($reviews));
//            }
//            $totalSize = $totalSize - SKILL_PHOTOS_ENTRY_LIMIT;
//            $startId = $startId + SKILL_PHOTOS_ENTRY_LIMIT;
//            $endId = $endId + SKILL_PHOTOS_ENTRY_LIMIT;
//        }
//
//
//        self::$LOG->debug("/** Exiting " . __METHOD__ . " **/");
//    }
//
//    public function test_fetchGeoTag()
//    {
//        self::$LOG->debug("/** Entering " . __METHOD__ . " **/");
//
//        $results = self::$sideKixDAO->fetchGeoTag(2);
//        self::$LOG->debug("geotag: ".$results);
//        $this->assertEquals("34.0500,118.2500", $results);
//
//
//        self::$LOG->debug("/** Exiting " . __METHOD__ . " **/");
//    }
//
//
//}
