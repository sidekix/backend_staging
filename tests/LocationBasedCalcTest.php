<?php

require __DIR__ . "/../src/database_functions/calculations/LocationBasedCalc.php";

class LocationBasedCalcTest extends PHPUnit_Framework_TestCase
{

    /**
     * test distanceGeo function
     */
    public function test_distanceGeo()
    {
        $coordinates1 = "34.0500,118.2500";
        $coordinates2 = "37.7833,122.4167";

        $d = (int)LocationBasedCalc::distanceGeo($coordinates1, $coordinates2);

        // Assert
        $this->assertEquals(347, $d);
    }

    public function test_distanceGeo_exceptions()
    {
        $coordinates1 = 4;
        $coordinates2 = "37.7833,122.4167";
        try {
            $d = (int)LocationBasedCalc::distanceGeo($coordinates1, $coordinates2);
        } catch (UnexpectedValueException $ex) {
            return;
        }

        $this->fail("Exception not raised as expected");

    }

    /**
     * test the distance function
     */
    public function test_distance()
    {
        $lat1 = 34.0500;
        $lon1 = 118.2500;

        $lat2 = 37.7833;
        $lon2 = 122.4167;

        $d = (int)LocationBasedCalc::distance($lat1, $lon1, $lat2, $lon2, "Miles");

        // Assert
        $this->assertEquals(347, $d);
    }

    /**
     * testing exceptions
     */
    public function test_distance_exceptions()
    {
        $lat1 = "34.0500";
        $lon1 = 118.2500;

        $lat2 = 37.7833;
        $lon2 = 122.4167;
        try {
            LocationBasedCalc::distance($lat1, $lon1, $lat2, $lon2, "Miles");
        } catch (UnexpectedValueException $ex) {
            return;
        }
        $this->fail("Exception not raised as expected");
    }
}
