<?php
/**
 * Created by PhpStorm.
 * User: jerry
 * Date: 1/24/15
 * Time: 1:30 PM
 */

require_once __DIR__ . "/../src/Config.php";
require_once __DIR__ . "/../src/objects/Photo.php";

class PhotoTest extends PHPUnit_Framework_TestCase {

    protected static $LOG;

    /*
     * Setup before test is run
     */
    public static function setUpBeforeClass()
    {
        self::$LOG = new SideKix_LOGGER(__CLASS__);
    }

    /*
     * tear down after tests are complete
     */
    public static function tearDownAfterClass()
    {
        //stub
    }

    public function test_fetchPhoto()
    {

        self::$LOG->debug("/** Entering " . __METHOD__ . " **/");

        $results = Photo::getPhotoWithId(11);
        self::$LOG->debug("results: " . $results);
        $this->assertEquals("11", $results->get_id());
        $this->assertEquals("5", $results->get_profile_id());
        $this->assertEquals("1420615390", $results->get_upload_time());
        $this->assertEquals("/data/fakePath/pic_5.jpg", $results->get_file_path());
        $this->assertEquals("34.0500,118.2500", $results->get_geotag());
        $this->assertEquals("1232", $results->get_views());
        $this->assertEquals("4452", $results->get_likes());

        self::$LOG->debug("/** Exiting " . __METHOD__ . " **/");

    }
}
