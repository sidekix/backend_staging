<?php
/**
 * Created by PhpStorm.
 * User: jerry
 * Date: 1/21/15
 * Time: 12:15 PM
 */

require __DIR__ . "/../src/aux_functions/HelperFuncs.php";

class HelperFuncsTest extends PHPUnit_Framework_TestCase {

    public function test_checkType()
    {
        $var = 5;

        try {
            HelperFuncs::checkType($var, "integer", __CLASS__, __METHOD__);
        } catch (InvalidArgumentException $ex) {
            $this->fail("Exception not raised as expected");;
        }

        $var = "word";
        try {
            HelperFuncs::checkType($var, "integer", __CLASS__, __METHOD__);
        } catch (InvalidArgumentException $ex) {
            return;
        }

        $this->fail("Exception not raised as expected");

    }

}
