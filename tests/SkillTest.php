<?php
/**
 * Created by PhpStorm.
 * User: jerry
 * Date: 1/24/15
 * Time: 7:22 AM
 */

require_once __DIR__ . "/../src/Config.php";
require_once __DIR__ . "/../src/objects/Skill.php";

class SkillTest extends PHPUnit_Framework_TestCase {

    protected static $LOG;

    /*
     * Setup before test is run
     */
    public static function setUpBeforeClass()
    {
        self::$LOG = new SideKix_LOGGER(__CLASS__);
    }

    /*
     * tear down after tests are complete
     */
    public static function tearDownAfterClass()
    {
        //stub
    }

    public function test_fetchSkill()
    {
        /*
         *results: {"0":"2","id":"2","1":"Washing","name":"Washing","2":"1","is_predefined":"1","3":"123232",
         * "time_created":"123232","4":"1","creator_profile_id":"1"}
         */
        self::$LOG->debug("/** Entering " . __METHOD__ . " **/");

        $results = Skill::getSkillWithId(2);
        self::$LOG->debug("results: " . $results);
        $this->assertEquals("2", $results->get_id());
        $this->assertEquals("Washing", $results->get_name());
        $this->assertEquals("1", $results->get_is_predefined());
        $this->assertEquals("1", $results->get_creator_profile_id());

        self::$LOG->debug("/** Exiting " . __METHOD__ . " **/");

    }
}
